# Instructions

Just run `run_ubuntu.sh` for on an Linux OS. Once you are inside the docker, you
can start SCONE (`sconestudio`). Scenario files are located in
`/home/user/SCONE/segmental_controller`.

We have separated the docker files into `ubuntu` and `mac` folders. In order to
access the `scone.deb` and controller files one has to build the docker using
the `--file` option (e.g., `docker build -t scone --file ubuntu/Dockerfile .`).
